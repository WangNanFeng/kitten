import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'
import axios from '../../http/request'
import { actionCreators } from '../login/store'
import { WhiteSpace, List, InputItem, WingBlank, Button, Toast, SegmentedControl, Picker } from 'antd-mobile'
import { rechargeType, operateType } from '../../config/config'
import config from '../../config/config'
import Header from '../common/header'

class SendCard extends Component {
  constructor(props) {
    super(props)
    this.state = {
      dataSource: [{ value: 26, label: '50元26张' }, { value: 55, label: '100元55张' }],
      rechargeUid: null,
      confirmRechargeUid: null,
      rechargeAmount: [26],
      rechargeType: rechargeType,
      operateType: operateType,
      gold: 0,
      diamond: 0,
      uName: '',
      type: 0,
      houseCard: [{ value: 26, label: '50元26张' }, { value: 55, label: '100元55张' }],
      catCard: [
        { value: 100000, label: '10万' },
        { value: 200000, label: '20万' },
        { value: 500000, label: '50万' },
        { value: 1000000, label: '100万' }
      ]
    }
  }

  rechargeUidChange = v => {
    console.log(v)
    this.setState({
      rechargeUid: v
    })
  }

  confirmRechargeUidChange = v => {
    this.setState({
      confirmRechargeUid: v
    })
  }

  getuName = () => {
    const { confirmRechargeUid, rechargeUid } = this.state
    if (confirmRechargeUid !== rechargeUid) {
      Toast.info('两次输入的代理商ID不一致,请重新输入')
      return
    }
    axios
      .get(config.baseURL + '/account', {
        params: {
          uid: this.state.confirmRechargeUid
        }
      })
      .then(res => {
        const resultCode = res.data.code
        if (resultCode === 0) {
          this.setState({
            uName: res.data.msg.name
          })
        } else if (resultCode === -1) {
          Toast.info('登录失效, 请重新登录 !!!', 1)
          this.props.logout()
        } else {
          Toast.fail(res.data.msg, 1)
        }
      })
  }

  rechargeAmountChange = v => {
    console.log(v)
    this.setState({
      rechargeAmount: v
    })
  }

  recharge = () => {
    const { rechargeUid, rechargeAmount, rechargeType, operateType } = this.state
    axios
      .get(config.baseURL + '/recharge2c', {
        params: {
          rechargeUid,
          rechargeAmount: rechargeAmount[0],
          rechargeType,
          operateType
        }
      })
      .then(res => {
        const resultCode = res.data.code

        if (resultCode === 0) {
          Toast.success('充值成功')
          this.getGold()
        } else if (resultCode === -1) {
          Toast.info('登录失效, 请重新登录 !!!', 1)
          this.props.logout()
        } else {
          Toast.fail(res.data.msg)
        }
      })
  }
  getGold = () => {
    const { uid } = this.props
    axios.get(config.baseURL + '/account/proxy', { params: { uid } }).then(res => {
      const resultCode = res.data.code
      if (resultCode === 0) {
        this.setState({
          gold: res.data.msg.gold,
          diamond: res.data.msg.diamond,
        })
      } else if (resultCode === -1) {
        Toast.info('登录失效, 请重新登录 !!!', 1)
        this.props.logout()
      } else {
        Toast.fail(res.data.msg)
      }
    })
  }

  handTypeChange = e => {
    const { houseCard, catCard } = this.state
    const type = e.nativeEvent.selectedSegmentIndex
    this.setState({
      type,
      dataSource: type === 0 ? houseCard : catCard,
      rechargeAmount: type === 0 ? [houseCard[0].value] : [catCard[0].value],
      rechargeType: type === 0 ? 0 : 1
    })
  }


  componentDidMount() {
    const self = this
    self.getGold()
    // demo 程序将粘贴事件绑定到 document 上
    document.addEventListener('paste', self.paste, false)
  }
  render() {
    if (this.props.loginStatus) {
      const { gold, uName, rechargeUid, confirmRechargeUid, rechargeAmount, type, dataSource,diamond } = this.state
      const {
        rechargeUidChange,
        confirmRechargeUidChange,
        rechargeAmountChange,
        recharge,
        getuName,
        handTypeChange
      } = this
      return (
        <Fragment>
          <Header>发放房卡</Header>
          <WhiteSpace />
          <WingBlank>
            <SegmentedControl selectedIndex={type} values={['发放房卡', '发放猫卡']} onChange={handTypeChange} />
          </WingBlank>
          <WhiteSpace />
          <List>
            <InputItem type="number" disabled value={type=== 0? gold: diamond}>
              当天剩余
            </InputItem>
            <WhiteSpace />
            <WhiteSpace />
            <InputItem type="number" placeholder="请输入用户数字ID" value={rechargeUid} onChange={rechargeUidChange}>
              用户数字ID
            </InputItem>
            <InputItem
              type="number"
              placeholder="请输入确认用户ID"
              value={confirmRechargeUid}
              onChange={confirmRechargeUidChange}
              onBlur={getuName}
            >
              确认用户ID
            </InputItem>
            <InputItem type="text" placeholder="请输入游戏昵称" value={uName} disabled>
              游戏昵称
            </InputItem>
            <Picker data={dataSource} cols={1} className="forss" onChange={rechargeAmountChange} value={rechargeAmount}>
              <List.Item arrow="horizontal">点击选择发放数量</List.Item>
            </Picker>
          </List>
          <WhiteSpace />
          <WingBlank>
            <Button type="primary" onClick={recharge}>
              发放
            </Button>
          </WingBlank>
        </Fragment>
      )
    } else {
      return <Redirect to="/login" />
    }
  }
  componentWillMount() {
    const self = this
    document.removeEventListener('paste', self.paste, false)
  }
}

const mapStateToProps = state => ({
  loginStatus: state.getIn(['login', 'login']),
  uid: state.getIn(['login', 'uid'])
})
const mapDispatchToProps = dispatch => ({
  logout() {
    dispatch(actionCreators.logout())
  }
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SendCard)
