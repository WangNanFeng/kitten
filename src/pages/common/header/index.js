import React, { Component } from 'react'
import { HeaderWrapper, BackButton, Title } from './style'

class Header extends Component {
  back = () => {
    window.history.go(-1)
  }

  render() {
    return (
      <div>
        <HeaderWrapper>
          {this.props.isHomePage ? '' : <BackButton onClick={this.back}>&lt; 返回</BackButton>}
          <Title>{this.props.children}</Title>
        </HeaderWrapper>
      </div>
    )
  }
}

export default Header
