import styled from 'styled-components'

export const HeaderWrapper = styled.div`
  position: relative;
  text-align: center;
  width: 100%;
  height: 50px;
  background-color: rgba(0, 0, 0, 0.8);
`

export const BackButton = styled.span`
  position: absolute;
  left: 20px;
  line-height: 50px;
  font-size: 18px;
  color: #fff;
`

export const Title = styled.span`
  line-height: 50px;
  font-size: 18px;
  color: #fff;
`
