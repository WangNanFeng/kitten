import styled from 'styled-components'

export const LoginWrapper = styled.div`
  /* z-index: 0; */
  /* position: absolute; */
  /* left: 0;
  right: 0;
  bottom: 0; */
  /* top: 56px; */
  /* background: #eee; */
`

export const CodePicWrapper = styled.div`
  width: 120px;
  height: 60px;
  img {
    width: 100%;
    height: 100%;
  }
`
