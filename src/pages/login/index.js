import React, { Component, Fragment } from 'react'
import { Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import md5 from 'md5'
import axios from '../../http/request'
import config from '../../config/config'
import { List, InputItem, WhiteSpace, WingBlank, Button, Toast, Flex } from 'antd-mobile'
import Header from '../common/header'
import { LoginWrapper, CodePicWrapper } from './style'
import { actionCreators } from './store'
import codePic from '../../statics/img/code.jpg'

class Login extends Component {
  state = {
    hasError: false,
    uid: '',
    pwd: '',
    captcha: '',
    captchaUrl: codePic
  }

  onErrorClick = () => {
    if (this.state.hasError) {
      Toast.info('请输入正确的手机号码')
    }
  }
  uidChange = uid => {
    if (uid.replace(/\s/g, '').length < 11) {
      this.setState({
        hasError: true
      })
    } else {
      this.setState({
        hasError: false
      })
    }
    this.setState({
      uid
    })
  }

  pwdChange = pwd => {
    this.setState({
      pwd
    })
  }

  captchaChange = captcha => {
    this.setState({
      captcha
    })
  }

  getCaptchaUrl = () => {
    axios.get(config.baseURL + '/captcha').then(res => {
      this.setState({
        captchaUrl: res.data
      })
    })
  }

  componentDidMount() {
    this.getCaptchaUrl()
  }

  render() {
    const { hasError, uid, pwd, captcha, captchaUrl } = this.state
    const { loginStatus, login } = this.props
    if (!loginStatus) {
      return (
        <Fragment>
          <Header>用户登录</Header>
          <LoginWrapper>
            <WhiteSpace />
            <List>
              <InputItem
                type="phone"
                placeholder="请输入手机号码"
                error={hasError}
                onErrorClick={this.onErrorClick}
                onChange={this.uidChange}
                value={uid}
                clear
              >
                手机号码
              </InputItem>

              <InputItem type="password" placeholder="****" onChange={this.pwdChange} value={pwd} clear>
                密码
              </InputItem>

              <InputItem type="text" placeholder="请输入验证码" onChange={this.captchaChange} value={captcha} clear>
                验证码
              </InputItem>
            </List>
            <WhiteSpace />
            <WingBlank>
              <Flex justify="center">
                <Flex.Item>
                  <span style={{ fontSize: '16px' }}>点击图片切换验证码</span>
                </Flex.Item>
                <Flex.Item>
                  <CodePicWrapper dangerouslySetInnerHTML={{ __html: captchaUrl }} onClick={this.getCaptchaUrl}>
                    {/* <img src={captchaUrl} alt="" /> */}
                  </CodePicWrapper>
                </Flex.Item>
              </Flex>
            </WingBlank>
            <WhiteSpace />
            <WingBlank>
              <Button type="primary" onClick={() => login(uid, pwd, captcha)}>
                登录
              </Button>
            </WingBlank>
          </LoginWrapper>
        </Fragment>
      )
    } else {
      return <Redirect to="/" />
    }
  }
}

const mapStateToProps = state => ({
  loginStatus: state.getIn(['login', 'login'])
})

const mapDispatchToProps = dispatch => ({
  login(uid, pwd, captcha) {
    dispatch(actionCreators.login(uid.replace(/\s/gi, ''), md5(pwd), captcha))
  }
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Login)
