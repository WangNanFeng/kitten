import React, { Component } from 'react'
import axios from '../../http/request'
import { Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import { actionCreators } from '../login/store'
import { List, WhiteSpace, Toast } from 'antd-mobile'
import Header from '../common/header'
import { UserCardWrapper } from './style'
import { formatDate } from '../../utils/date'
import config from '../../config/config'

const Item = List.Item

class UseCard extends Component {
  constructor(props) {
    super(props)
    this.state = {
      consumeCount: 0,
      catCount: 0
    }
  }
  componentDidMount() {
    this.queryConsume()
    this.queryCat()
  }

  queryConsume = () => {
    const date = new Date()
    axios
      .get(config.baseURL + '/query/consume', {
        params: {
          uid: this.props.uid,
          type: 0,
          startTime: formatDate(date, 'yyyy-MM-dd') + ' 00:00:00',
          endTime: formatDate(date, 'yyyy-MM-dd') + ' 24:00:00'
        }
      })
      .then(res => {
        const resultCode = res.data.code
        if (resultCode === 0) {
          this.setState({
            consumeCount: res.data.msg
          })
        } else if (resultCode === -1) {
          Toast.info('登录失效, 请重新登录 !!!', 1)
          this.props.logout()
        } else {
          Toast.fail(res.data.msg)
        }
      })
  }

  queryCat = () => {
    const date = new Date()
    axios
      .get(config.baseURL + '/query/consume', {
        params: {
          uid: this.props.uid,
          type: 1,
          startTime: formatDate(date, 'yyyy-MM-dd') + ' 00:00:00',
          endTime: formatDate(date, 'yyyy-MM-dd') + ' 24:00:00'
        }
      })
      .then(res => {
        const resultCode = res.data.code
        if (resultCode === 0) {
          this.setState({
            consumeCount: res.data.msg
          })
        } else if (resultCode === -1) {
          Toast.info('登录失效, 请重新登录 !!!', 1)
          this.props.logout()
        } else {
          Toast.fail(res.data.msg)
        }
      })
  }

  render() {
    const { loginStatus } = this.props
    if (loginStatus) {
      return (
        <UserCardWrapper>
          <Header>当天消耗量</Header>
          <WhiteSpace />
          <List>
            <Item>
              使用房卡数量:
              {this.state.consumeCount}
            </Item>
            <Item>
              使用猫币数量:
              {this.state.catCount}
            </Item>
          </List>
        </UserCardWrapper>
      )
    } else {
      return <Redirect to="login" />
    }
  }
}

const mapStateToProps = state => ({
  loginStatus: state.getIn(['login', 'login']),
  uid: state.getIn(['login', 'uid'])
})
const mapDispatchToProps = dispatch => ({
  logout() {
    dispatch(actionCreators.logout())
  }
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UseCard)
