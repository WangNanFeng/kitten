import { fromJS } from 'immutable'
import * as constants from './constants'

const defaultState = fromJS({
  gold: 0,
  uType: 0
})

export default (state = defaultState, action) => {
  switch (action.type) {
    case constants.CHANGE_USER_INFO:
      return state.merge({ gold: action.gold, uType: action.uType })
    default:
      return state
  }
}
