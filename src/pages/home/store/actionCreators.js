import * as constants from './constants'
import axios from '../../../http/request'
import { Toast } from 'antd-mobile'
import config from '../../../config/config'

const changeUserInfo = (gold, uType) => ({
  type: constants.CHANGE_USER_INFO,
  gold,
  uType
})

export const getUseInfo = uid => {
  return dispatch =>
    axios.get(config.baseURL + '/account/proxy', { params: { uid } }).then(res => {
      const resultCode = res.data.code
      if (resultCode === 0) {
        dispatch(changeUserInfo(res.data.msg.gold, res.data.msg.uType))
      } else if (resultCode === 1) {
        Toast.info(res.data.msg)
      } else {
        Toast.info('登陆失败')
      }
    })
}
