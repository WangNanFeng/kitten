import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import axios from '../../http/request'
import { List, InputItem, Toast, WhiteSpace, WingBlank, Button } from 'antd-mobile'
import Header from '../common/header'
import config from '../../config/config'
import { actionCreators } from '../login/store'

class CreateUser extends Component {
  constructor(props) {
    super(props)
    this.state = {
      uid: '',
      pwd: '',
      uName: ''
    }
  }

  uidChange = uid => {
    this.setState({
      uid
    })
  }

  pwdChange = pwd => {
    this.setState({
      pwd
    })
  }

  uNameChange = uName => {
    this.setState({
      uName
    })
  }

  createUser = (uid, pwd, uName) => {
    axios
      .get(config.baseURL + '/register', {
        params: {
          uid: uid.replace(/\s+/gi, ''),
          pwd,
          uName
        }
      })
      .then(res => {
        const resultCode = res.data.code
        if (resultCode === 0) {
          Toast.success(res.data.msg, 1)
        } else if (resultCode === -1) {
          Toast.info('登录失效, 请重新登录 !!!', 1)
          this.props.logout()
        } else {
          Toast.fail(res.data.msg, 1)
        }
      })
  }

  render() {
    const { uid, pwd, uName } = this.state
    const { loginStatus } = this.props

    if (loginStatus) {
      return (
        <div>
          <Header>创建用户</Header>
          <List renderHeader={() => '修改密码'}>
            <InputItem type="number" placeholder="请输入手机号" onChange={this.uidChange} value={uid} clear>
              手机号
            </InputItem>
            <InputItem type="text" placeholder="输入密码" onChange={this.pwdChange} value={pwd} clear>
              密码
            </InputItem>
            <InputItem type="text" placeholder="请输入代理商昵称" onChange={this.uNameChange} value={uName} clear>
              代理商昵称
            </InputItem>
          </List>
          <WhiteSpace />
          <WingBlank>
            <Button type="primary" onClick={() => this.createUser(uid, pwd, uName)}>
              创建用户
            </Button>
          </WingBlank>
        </div>
      )
    } else {
      return <Redirect to="/login" />
    }
  }
}

const mapStateToProps = state => ({
  loginStatus: state.getIn(['login', 'login'])
})

const mapDispatchToProps = dispatch => ({
  logout() {
    dispatch(actionCreators.logout())
  }
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CreateUser)
